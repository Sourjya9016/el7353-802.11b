#!/bin/bash
ssh -o "StrictHostKeyChecking no" root@"$1" "pkill iperf"
ssh -o "StrictHostKeyChecking no" root@"$1" "nohup iperf -s > iperf.out 2> iperf.err < /dev/null &";
STR="$2"
while [ -n "$3" ]; do
echo "$3"
	ssh -o "StrictHostKeyChecking no" root@"$3" "mkdir "$STR"";
	ssh -o "StrictHostKeyChecking no" root@"$3" "iwconfig wlan0 rate 2M";
	ssh -o "StrictHostKeyChecking no" root@"$3" "sh -c 'cd ~/"$STR"/;nohup iperf -c 192.168.12.1 -u -b 2M -t 120  > iperf"$3".out 2> iperf.err < /dev/null &'";

shift

done

